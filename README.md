# Mage2 Module Eorder ProductBrandData

    ``eorder/magento2-module-product-brand-data``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Attributes](#markdown-header-attributes)
 - [Usage](#markdown-header-usage)


## Main Functionalities
Magento2 Module adding GraphQL Query for Attributes of Type Select and Multiselect

## Installation

### Type 1: Zip file

 - Unzip the zip file in `app/code/Eorder`
 - Enable the module by running `php bin/magento module:enable Eorder_ProductBrandData`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Add the composer repository to the configuration
<pre>"repositories": {
           "eorder": {
               "type": "package",
               "package": {
                   "name": "eorder/magento2-module-product-brand-data",
                   "version": "1.1.0",
                   "type": "package",
                   "source": {
                       "url": "https://gitlab.com/cfischer26/magento2-module-product-brand-data.git",
                       "type": "git",
                       "reference": "master"
                   }
               }
           }
   </pre>        
 - Install the module composer by running `composer require magento2-module-product-brand-data`
 - enable the module by running `php bin/magento module:enable Eorder_ProductBrandData`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

## Attributes

 - Product - Brand (brand)

## Usage

 - You can get all Options for attribute brand with the following GraphQL Query
 <pre>{
  select_product_attribute_values(
    attribute_code: "modtest"
  ) {
      options {
      code,
      value
    }
  }
}</pre>
