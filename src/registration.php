<?php
/**
 * @author        Christoph Fischer <cfischer@nextorder.de>
 * @copyright     Copyright (c) 2021 eORDER GmbH (https://www.nextorder.de)
 */

declare(strict_types=1);

use \Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Eorder_ProductBrandData', __DIR__);

