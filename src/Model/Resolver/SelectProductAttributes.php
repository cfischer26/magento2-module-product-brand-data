<?php
/**
 * @author        Christoph Fischer <cfischer@nextorder.de>
 * @copyright     Copyright (c) 2021 eORDER GmbH (https://www.nextorder.de)
 */
declare(strict_types=1);

namespace Eorder\ProductBrandData\Model\Resolver;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Catalog\Api\ProductAttributeRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class SelectProductAttributes
 * @package Eorder\ModTest\Model\Resolver
 */
class SelectProductAttributes implements ResolverInterface
{

    /**
     * @var ProductAttributeRepositoryInterface
     */
    private ProductAttributeRepositoryInterface $productAttributeRepository;

    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;

    /**
     * SelectProductAttributes constructor.
     * @param ProductAttributeRepositoryInterface $productAttributeRepository
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ProductAttributeRepositoryInterface $productAttributeRepository,
        StoreManagerInterface $storeManager
    ) {
        $this->productAttributeRepository = $productAttributeRepository;
        $this->storeManager = $storeManager;
    }

    /**
     * @param Field $field
     * @param $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        $currentStore = $this->storeManager->getStore()->getId();
        $attribute = $this->productAttributeRepository->get($args['attribute_code']);
        $this->storeManager->setCurrentStore(0);
        $adminOptions = $attribute->getSource()->getAllOptions();
        $this->storeManager->setCurrentStore($currentStore);
        $result = ['options' => []];
        if ($attribute->getFrontendInput() === 'select' || $attribute->getFrontendInput() === 'multiselect') {
            foreach ($attribute->getOptions() as $key => $option) {
                if (!empty(trim($option->getLabel()))) {
                    $result['options'][] = [
                        'code' => $adminOptions[$key]['label'],
                        'value' => $option->getLabel()
                    ];
                }
            }
        } else {
            throw new LocalizedException(__('Attribute must be of type select or multiselect.'));
        }

        return $result;
    }

}
